# To Do List MERN App

Esta aplicación web fue creada con el proposito de ser utilizada en el proyecto del primer parcial de la clase de Cloud Computing desarrollada por Humberto Alejandro Navarro Andujo.

## Objetivo

El objetivo es implementar la arquitectura de una aplicación web utilizando multiples instancias del servicio de ec2 de aws para correr el backend y utilizando el servicio s3 de aws para hostear el sitio web estatico.

![](./diagram_cloud_aws.png)